import { Component, ViewContainerRef } from '@angular/core';
import { CustomersService } from './customers.service';
import { AuthenticationService } from './authentication.service';
import eCustomer = Consultingwerk.SmartComponentsDemo.OERA.Sports2000.CustomerBusinessEntity.eCustomer;
import {  } from 'rxjs/operators';
import { Filter } from './filter';
import { BehaviorSubject } from 'rxjs';
import { NotificationService,  } from '@progress/kendo-angular-notification';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  customers: BehaviorSubject<eCustomer[]>;

  searchPhrase: string;

  selectedCustomer: eCustomer = {};

  constructor(private customersService: CustomersService,
              private authService: AuthenticationService,
              private notificationsService: NotificationService,
              private vcRef: ViewContainerRef) {
                this.customers = this.customersService.customers;
  }

  ngOnInit() {
    this.authService.login('demo', 'demo')
        .subscribe(() => this.fetch());
  }

  fetch(filter?: Filter[]) {
    this.customersService.fetch(filter);
  }

  onFilterButtonClicked() {
    this.fetch([{
      field: 'Name',
      operator: 'begins',
      value: this.searchPhrase
    }]);
  }

  updateCustomer() {
    this.customersService.update(this.selectedCustomer)
        .subscribe(() => {
          this.notificationsService.show({
            content: 'Your changes have been saved successfully.',
            appendTo: this.vcRef,
            type: {
              style: 'success'
            }
          })
        }, (err) => {
          this.notificationsService.show({
            content: err.message || 'There was an error while saving your changes.',
            type: {
              style: 'error'
            },
            appendTo: this.vcRef
          });
        });
  }

}
