import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import eCustomer = Consultingwerk.SmartComponentsDemo.OERA.Sports2000.CustomerBusinessEntity.eCustomer;
import { BehaviorSubject, Observable } from 'rxjs';
import { Filter } from './filter';
import { API_URL } from './config';

@Injectable()
export class CustomersService {

  private _customers: eCustomer[] = [];
  public customers: BehaviorSubject<eCustomer[]> = new BehaviorSubject(this._customers);

  constructor(private http: HttpClient) {

  }

  public fetch(filters?: Filter[]): Observable<eCustomer[]> {
    let url: string = API_URL;
    if (filters && filters.length > 0) {
      url += `?${this.filtersToString(filters)}`;
    }
    this.http.get<eCustomer[]>(url).subscribe(result => {
      this._customers = result;
      this.customers.next(this._customers);
    });
    return this.customers.asObservable();
  }

  public update(customer: any): Observable<any> {
    return this.http.put(`${API_URL}/${customer.id}`, customer);
  }

  private filtersToString(filters: Filter[]): string {
    let filterString = '';
    filters.forEach((filter, index) => {
      filterString += `${filter.field}[${filter.operator}]${filter.value}`;
      if (index < filters.length - 1) {
        filterString += '&';
      }
    });
    return filterString;
  }

}
