export const BASE_URL: string = 'http://localhost:8820'
export const API_URL: string = `${BASE_URL}/web/Entities/Customers`;
export const AUTH_URL: string = `${BASE_URL}/static/auth/j_spring_security_check`;
