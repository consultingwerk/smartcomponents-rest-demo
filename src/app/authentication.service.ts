import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AUTH_URL } from './config';
import { Observable } from 'rxjs';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  public login(username: string, password: string): Observable<any> {
    const formData: HttpParams = new HttpParams({
      fromObject: {
        j_username: username,
        j_password: password,
        submit: 'Login'
      }
    });

    const headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this.http.post(AUTH_URL, formData, { headers: headers });
  }

}
