export interface Filter {
    value: string,
    operator: string,
    field: string
}